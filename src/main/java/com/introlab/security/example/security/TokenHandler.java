package com.introlab.security.example.security;

import com.google.common.io.BaseEncoding;
import com.introlab.security.example.dao.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

@Component
public class TokenHandler {

    private final SecretKey secretKey;

    public TokenHandler() {
        String jwtKey = "secret";
        byte[] decodeKey = BaseEncoding.base64().decode(jwtKey);
        this.secretKey = new SecretKeySpec(decodeKey, 0, decodeKey.length, "AES");
    }

    Optional<String> extractUserId(@NotNull String token) {
        try {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            Claims body = claimsJws.getBody();
            return Optional
                    .ofNullable(body.getId());
        } catch (RuntimeException ex) {
            return Optional.empty();
        }
    }

    public String generateToken(User user, LocalDateTime expires) {
        return Jwts.builder()
                .setId(user.getId())
                .setExpiration(Date.from(expires.atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }
}
