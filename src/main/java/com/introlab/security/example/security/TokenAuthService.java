package com.introlab.security.example.security;

import com.introlab.security.example.dao.entity.TokenStorage;
import com.introlab.security.example.dao.entity.User;
import com.introlab.security.example.dao.entity.UserIpAddr;
import com.introlab.security.example.dao.service.TokenStorageService;
import com.introlab.security.example.dao.service.UserIpAddrService;
import com.introlab.security.example.dao.service.UserService;
import com.introlab.security.example.domain.UserAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.isNull;

@Service
public class TokenAuthService {

    @Autowired
    private UserService userService;
    @Autowired
    private TokenHandler tokenHandler;
    @Autowired
    private TokenStorageService storageService;
    @Autowired
    private UserIpAddrService addrService;

    private static final String AUTH_HEADER_NAME = "X-Auth-Token";

    Optional<Authentication> getAuthentication(@NotNull HttpServletRequest request) {
        String t = request.getHeader(AUTH_HEADER_NAME);
        String ip = request.getRemoteAddr();
        return Optional
                .ofNullable(t)
                .flatMap(tokenHandler::extractUserId)
                .filter(id -> {
                    Optional<UserIpAddr> userIpAddr = addrService.findById(id);
                    return userIpAddr.filter(uia -> uia.getAddrs().contains(ip)).isPresent();
                })
                .filter(id -> {
                    Optional<TokenStorage> st = storageService.findById(id);
                    return st.filter(tokenStorage -> Objects.equals(t, tokenStorage.getToken())).isPresent();
                })
                .map(userId -> {
                    UserAuth authentication = (UserAuth) SecurityContextHolder.getContext().getAuthentication();
                    if (isNull(authentication)) {
                        Optional<User> user = userService.loadUserById(userId);
                        if (user.isPresent()) {
                            return new UserAuth(user.get());
                        }
                    } else {
                        if (Objects.equals(authentication.getUser().getId(), userId)) {
                            return authentication;
                        } else {
                            Optional<User> user = userService.loadUserById(userId);
                            if (user.isPresent()) {
                                return new UserAuth(user.get());
                            }
                        }

                    }
                    return null;
                });
    }
}
