package com.introlab.security.example.dao.repository;

import com.introlab.security.example.dao.entity.UserIpAddr;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserIpAddrRepository extends MongoRepository<UserIpAddr, String> {
}
