package com.introlab.security.example.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@Document(collection = "userIpAddr")
public class UserIpAddr {

    @Id
    private String id;
    private List<String> addrs;
}
