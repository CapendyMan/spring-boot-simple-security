package com.introlab.security.example.dao.service;

import com.introlab.security.example.dao.entity.UserIpAddr;
import com.introlab.security.example.dao.repository.UserIpAddrRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserIpAddrService {

    @Autowired
    private UserIpAddrRepository repository;

    public Optional<UserIpAddr> findById(String id) {
        return repository.findById(id);
    }

    public void save(UserIpAddr userIpAddr) {
        repository.save(userIpAddr);
    }
}
