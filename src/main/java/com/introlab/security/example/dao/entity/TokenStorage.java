package com.introlab.security.example.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tokenStorage")
@AllArgsConstructor
@Data
public class TokenStorage {

    @Id
    private String id;
    private String token;
}
