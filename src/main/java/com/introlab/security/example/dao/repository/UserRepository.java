package com.introlab.security.example.dao.repository;

import com.introlab.security.example.dao.entity.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByLogin(String login);
    Optional<User> findById(String id);
}
