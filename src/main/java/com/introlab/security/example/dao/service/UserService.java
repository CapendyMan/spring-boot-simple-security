package com.introlab.security.example.dao.service;

import com.introlab.security.example.dao.entity.User;
import com.introlab.security.example.dao.repository.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public User loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByLogin(s)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with login: " + s));
    }

    @Transactional
    public Optional<User> loadUserById(@NotNull String id) {
        return userRepository.findById(id);
    }

    @Transactional
    public void save(User user) {
        userRepository.save(user);
    }
}
