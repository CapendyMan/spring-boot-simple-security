package com.introlab.security.example.dao.repository;

import com.introlab.security.example.dao.entity.TokenStorage;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TokenStorageRepository extends MongoRepository<TokenStorage, String> {
}
