package com.introlab.security.example.dao.service;

import com.introlab.security.example.dao.entity.TokenStorage;
import com.introlab.security.example.dao.repository.TokenStorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TokenStorageService {

    @Autowired
    private TokenStorageRepository repository;

    public Optional<TokenStorage> findById(String login) {
        return repository.findById(login);
    }

    public void save(TokenStorage tokenStorage) {
        repository.save(tokenStorage);
    }
}
