package com.introlab.security.example.controller;

import com.introlab.security.example.dao.entity.TokenStorage;
import com.introlab.security.example.dao.entity.User;
import com.introlab.security.example.dao.entity.UserIpAddr;
import com.introlab.security.example.dao.service.TokenStorageService;
import com.introlab.security.example.dao.service.UserIpAddrService;
import com.introlab.security.example.dao.service.UserService;
import com.introlab.security.example.payload.ApiResponse;
import com.introlab.security.example.payload.LoginRequest;
import com.introlab.security.example.payload.SignUpRequest;
import com.introlab.security.example.security.TokenHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;

import static java.util.Objects.isNull;

@RestController
public class BasicRestController {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserService service;
    @Autowired
    TokenHandler tokenHandler;
    @Autowired
    TokenStorageService tokenService;
    @Autowired
    UserIpAddrService addrService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/admin")
    public String adminPage() {
        return "Admin page";
    }

    @GetMapping("/")
    public String mainPage() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (Objects.equals(authentication.getPrincipal(), "anonymousUser")) {
            return "Hi anonymous user.";
        }

        User user = (User) authentication.getPrincipal();
        return "Hi " + user.getName();
    }

    @PostMapping("/token")
    public String getToken(@Valid @RequestBody LoginRequest request, HttpServletRequest servletRequest) {
        User user = service.loadUserByUsername(request.getLogin());

        if (isNull(user)) {
            return "error1";
        }

        if (!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            return "error2";
        }

        String token = tokenHandler.generateToken(user, LocalDateTime.now().plusDays(7));

        tokenService.save(new TokenStorage(user.getId(), token));

        Optional<UserIpAddr> userIpAddr = addrService.findById(user.getId());
        List<String> ips;

        if (userIpAddr.isPresent()) {
            ips = userIpAddr.get().getAddrs();
            if (!ips.contains(servletRequest.getRemoteAddr())) {
                ips.add(servletRequest.getRemoteAddr());
            }
        } else {
            ips = Collections.singletonList(servletRequest.getRemoteAddr());
        }

        addrService.save(new UserIpAddr(user.getId(), ips));

        return token;
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        User user = new User(signUpRequest.getLogin(), signUpRequest.getPassword(), Collections.unmodifiableSet(signUpRequest.getRoles()), signUpRequest.getName());

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        service.save(user);

        return ResponseEntity.ok(new ApiResponse(true, "User registered successfully"));
    }
}

